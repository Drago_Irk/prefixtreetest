import javax.faces.bean.ManagedBean;
import java.util.List;

@ManagedBean
public class AutoCompleteView {
    private String word;
    private PrefixTree tree;

    public AutoCompleteView() {
        tree = new PrefixTree();

        tree.put("кит");
        tree.put("ком");
        tree.put("кот");
        tree.put("корова");
        tree.put("косатка");
        tree.put("крыша");
        tree.put("корона");
    }

    public List<String> completeText(String query) {
        return tree.getWords(query, 4);
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }
}
