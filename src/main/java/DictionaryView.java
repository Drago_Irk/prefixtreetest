import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.List;

@ManagedBean
@ViewScoped
public class DictionaryView implements Serializable {
//    @Inject
//    private WordService service;

    private List<Word> words;
    private Word selectedWord;
    private Word oldWord;

    public Word getSelectedWord() {
        return selectedWord;
    }

    public void setSelectedWord(Word selectedWord) {
        this.selectedWord = selectedWord;
    }

    @ManagedProperty("#{wordService}")
    private WordService service;

    @PostConstruct
    public void init() {
        words = service.createWords(15);
    }

    public List<Word> getWords() {
        return words;
    }

    public void save() {
        words.remove(selectedWord);
        words.add(selectedWord);
        //TODO тут сохранение в базу должно было быть
        service.saveWord(selectedWord);
    }

    public void cancel() {
        selectedWord.restoreFrom(oldWord);
    }

    public void delete(Word word) {
        words.remove(word);
    }

    public void initAdd() {
        selectedWord = new Word();
    }

    public void initEdit() throws CloneNotSupportedException {
        oldWord = (Word)selectedWord.clone();
    }

    public void setService(WordService service) {
        this.service = service;
    }
}
