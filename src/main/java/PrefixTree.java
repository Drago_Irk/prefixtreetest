import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

class PrefixTree {
    private TreeNode root = new TreeNode();

    void put(String word) {
        TreeNode n = root;
        for (char ch : word.toLowerCase().toCharArray()) {
            if (!n.children.containsKey(ch)) {
                n.children.put(ch, new TreeNode());
            }
            n = n.children.get(ch);
        }
        n.isLeaf = true;
    }

    List<String> getWords(String word, int maxCnt) {
        TreeNode n = root;
        int idx = -1;
        char[] arr = word.toLowerCase().toCharArray();
        List<String> words = new ArrayList<>(maxCnt);

        for (char ch : arr) {
            if (!n.children.containsKey(ch)) {
                return words;
            } else {
                n = n.children.get(ch);
            }
            idx++;
        }

        if (idx > -1) {
            goDeeper(n, n, word.substring(0, idx + 1), words, maxCnt);
        }

        return words;
    }

    private void goDeeper(TreeNode start, TreeNode current, String word, List<String> words, int maxCnt) {
        if (current.isLeaf && current != start) {
            words.add(word);
        }
        current.children.forEach((key, value) -> {
            if (words.size() < maxCnt) {
                goDeeper(start, value, word + key, words, maxCnt);
            }
        });
    }

    boolean find(String word) {
        TreeNode n = root;
        for (char ch : word.toLowerCase().toCharArray()) {
            if (!n.children.containsKey(ch)) {
                return false;
            } else {
                n = n.children.get(ch);
            }
        }
        return true;
    }
}

class TreeNode {
    Map<Character, TreeNode> children = new TreeMap<>();
    boolean isLeaf;
}
