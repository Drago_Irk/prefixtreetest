import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(schema="test", name="words")
@SequenceGenerator(
        name = "id_generator",
        allocationSize = 1,
        schema = "test",
        sequenceName = "word_id_sequence")
public class Word implements Cloneable, Serializable {
    private static final long serialVersionUID = 2740220753968250227L;

    @Version
    @NotNull
    @Column(name = "opt_lock", nullable = false)
    private Long version;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_generator")
    @NotNull
    @Column(name="word_id", nullable = false, unique = true)
    private Long id;

    @NotNull
    @Column(name="value", nullable = false, unique = true)
    private String text;

    @NotNull
    @Column(name="status", nullable = false)
    private Boolean status = true;

    public Word() {
    }

    public Word(String text) {
        this.text = text;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Boolean getStatus() {
        return status;
    }

    public String getStatusText() {
        return status ? "ACTIVE" : "INACTIVE";
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public int getLength() {
        return text.length();
    }

    public void restoreFrom(Word anotherWord) {
        if (anotherWord != null) {
            this.setText(anotherWord.getText());
            this.setStatus(anotherWord.getStatus());
        }
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Word)) return false;
        Word word = (Word) o;
        return Objects.equals(id, word.id) &&
                Objects.equals(text, word.text) &&
                Objects.equals(status, word.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, text, status);
    }
}
