public interface WordRepository {
    Word getWordByText(String text);

    Word saveWord(Word w);

    void deleteWord(Word w);
}
