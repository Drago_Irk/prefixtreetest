import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@ApplicationScoped
public class WordRepositoryImpl implements WordRepository{
    @PersistenceContext(unitName = "main")
    private EntityManager em;

    public WordRepositoryImpl() {
    }

    @Override
    public Word getWordByText(String text) {
        return null;
    }

    @Override
    public Word saveWord(Word w) {
        return null;
    }

    @Override
    public void deleteWord(Word w) {

    }
}
