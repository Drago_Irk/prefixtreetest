import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import java.util.ArrayList;
import java.util.List;

@ManagedBean(name = "wordService")
@ApplicationScoped
public class WordService {
//    @Inject
//    WordRepository repository;

    public WordService() {
    }

    public List<Word> createWords(int size) {
        List<Word> list = new ArrayList<Word>();
        for(int i = 0 ; i < size ; i++) {
            list.add(new Word("Слово" + String.valueOf(i)));
        }

        return list;
    }

    public void saveWord(Word w) {
        //repository.savaeWord(w);
    }
}
