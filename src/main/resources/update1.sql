CREATE SCHEMA test;
CREATE ROLE sirius;

CREATE SEQUENCE test.word_id_sequence;
COMMENT ON SEQUENCE test.word_id_sequence
  IS 'Последовательность для генерации идентификаторов слов.';

CREATE TABLE test.words (
     word_id             bigint               NOT NULL DEFAULT nextval('test.word_id_sequence'),
     opt_lock            bigint               NOT NULL DEFAULT 0,
     value               text                 NOT NULL,
     status              boolean              NOT NULL DEFAULT FALSE
);

ALTER TABLE test.words
  ADD CONSTRAINT words_primary_key
  PRIMARY KEY (word_id);

ALTER TABLE test.words
  ADD CONSTRAINT words_value_unique
  UNIQUE (value);

CREATE INDEX words_value_idx ON test.words(value);

GRANT USAGE ON SCHEMA test TO sirius;
ALTER SEQUENCE test.word_id_sequence OWNER TO sirius;
ALTER TABLE test.words OWNER TO sirius;
