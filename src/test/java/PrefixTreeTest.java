import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class PrefixTreeTest {
    @Test
    public void put() {
        PrefixTree tree = new PrefixTree();
        tree.put("кит");

        Assert.assertTrue(tree.find("кит"));
    }

    @Test
    public void getWords() {
        PrefixTree tree = new PrefixTree();

        tree.put("кит");
        tree.put("ком");
        tree.put("кот");
        tree.put("корова");
        tree.put("косатка");
        tree.put("крыша");
        tree.put("корона");

        List<String> expected = Arrays.asList("ком", "корова", "корона", "косатка");
        List<String> actual  = tree.getWords("ко", 4);

        assertThat(actual, is(expected));
    }

    @Test
    public void find() {
        PrefixTree tree = new PrefixTree();

        tree.put("кит");
        tree.put("кот");
        tree.put("тест");

        Assert.assertTrue(tree.find("кит"));
        Assert.assertTrue(tree.find("кот"));
        Assert.assertTrue(tree.find("тест"));
        Assert.assertTrue(tree.find("ТеСт"));
        Assert.assertFalse(tree.find("кошка"));
        Assert.assertFalse(tree.find("тик"));
        Assert.assertTrue(tree.find("ко"));
    }
}
